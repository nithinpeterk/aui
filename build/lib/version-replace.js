'use strict';

var gulpTap = require('gulp-tap');
var pkg = require('../../package.json');

module.exports = function () {
    return gulpTap(function (file) {
        var code = file.contents.toString().replace(/\$\{project\.version\}/g, pkg.version);
        file.contents = new Buffer(code);
    });
};
