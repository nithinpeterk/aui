'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.series(
    gat.load('dist'),
    gat.load('docs/assets'),
    gat.load('docs/less-assets'),
    gat.load('docs/metalsmith'),
    gat.load('docs/js'),
    gat.load('docs/less')
);
