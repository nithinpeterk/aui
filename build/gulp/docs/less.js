'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var gulpLess = require('gulp-less');
var lazyPipe = require('lazypipe');
var minifyCss = require('../../lib/minify-css');
var minimatch = require('minimatch');
var path = require('path');

var opts = gat.opts();
var pathAssets = ['src/css-vendor/jquery/plugins/*.png', 'src/css-vendor/jquery/plugins/*.gif'];
var pathSrc = 'docs/src/styles/index.less';
var allPaths = [pathSrc].concat(pathAssets).concat(pathAssets.map(function (p) {
    return path.join(opts.root, p);
}));

module.exports = function docsLess () {
    var shouldMinify = !opts['no-minify'];

    var processLess = lazyPipe()
        .pipe(gulpDebug, {title: 'docs/less/compile'})
        .pipe(galv.cache, 'less', gulpLess({paths: [path.basename(pathSrc)]}))
        .pipe(gulpIf, shouldMinify, gulpDebug({title: 'docs/less/optimise'}))
        .pipe(gulpIf, shouldMinify, minifyCss());

    return gulp.src(allPaths)
        .pipe(gulpIf(function (file) { return minimatch(file.path, '**/*.less'); }, processLess()))
        .pipe(gulp.dest('.tmp/docs/dist/styles'));
};
