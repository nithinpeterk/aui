'use strict';

import include from '../../../src/js/aui/include';

describe('aui/include', function () {
    it('globals', function () {
        expect(AJS.include).to.equal(include);
    });
});
