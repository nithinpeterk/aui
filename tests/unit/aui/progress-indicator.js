'use strict';

import progressBars from '../../../src/js/aui/progress-indicator';

describe('aui/progress-indicator', function () {
    it('globals', function () {
        expect(AJS.progressBars).to.equal(progressBars);
    });
});
