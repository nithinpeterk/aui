'use strict';

import escape from '../../../src/js/aui/escape';

describe('aui/escape', function () {
    it('globals', function () {
        expect(AJS.escape).to.equal(escape);
    });

    it('API', function () {
        expect(escape('\u1234')).to.equal('ሴ');
        expect(escape('\u2235 hello')).to.equal('∵%20hello');
        expect(escape('\u2235 hello  你好 ')).to.equal('∵%20hello%20%20你好%20');
     });
});
