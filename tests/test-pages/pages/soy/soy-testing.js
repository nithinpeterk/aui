AJS.$(function() {

    var $container = AJS.$('#soy-test');

    var $simpleContainer = AJS.$('<section id="simple-calls"><h3>Simple (only required attributes specified)</h3></section>').appendTo($container);

    $simpleContainer
        .append(aui.page.pagePanel({
            content: '<div>AUI Panel</div>'
        }))
        .append(aui.page.pagePanel({
            content:
                aui.page.pagePanelNav({
                    content: 'Page Panel Nav'
                }) +
                aui.page.pagePanelContent({
                    content: 'Page Panel Content'
                }) +
                aui.page.pagePanelSidebar({
                    content: 'Page Panel Sidebar'
                }) +
                aui.page.pagePanelItem({
                    content: 'Page Panel Item'
                })
        }))
        .append(aui.page.pageHeader({
            content:
                aui.page.pageHeaderImage({
                    content: 'Image'
                }) +
                aui.page.pageHeaderMain({
                    content: '<h1>Page Header Main</h1>'
                }) +
                aui.page.pageHeaderActions({
                    content: 'Actions'
                })
        }))
        .append(aui.group.group({
            content: aui.group.item({content: '<div>AUI Item in AUI Group</div>', id:'soy-item'})
        }))
        .append(aui.table({
            content: '<tr><td>AUI Table</td></tr>'
        }))
        .append(aui.tabs({
            menuItems: [{
                isActive: true,
                url: '#tab1',
                text: 'Tab 1'
            }, {
                url: '#tab2',
                text: 'Tab 2'
            }],
            paneContent: aui.tabPane({
                isActive: true,
                content: 'Tab 1 Content'
            }) + aui.tabPane({
                content: 'Tab 2 Content'
            })
        }))
        .append(aui.dropdown.parent({
            content: aui.dropdown.trigger({
                accessibilityText: 'AUI Dropdown Trigger'
            }) + aui.dropdown.menu({
                content: aui.dropdown.item({
                    text: 'AUI Dropdown Item'
                })
            })
        }))
        .append(aui.toolbar.toolbar({
            content: aui.toolbar.split({
                split: 'left',
                content: aui.toolbar.group({
                    content: aui.toolbar.button({
                        text: 'AUI Button (text)'
                    }) + aui.toolbar.button({
                        title: 'AUI Button (icon)',
                        iconClass: 'my-toolbar-icon'
                    }) + aui.toolbar.link({
                        url: '#link',
                        text: 'AUI Link'
                    }) + aui.toolbar.dropdown({
                        text: 'AUI Toolbar Dropdown',
                        dropdownItemsContent: aui.dropdown.item({text: 'AUI Dropdown Item'})
                    }) + aui.toolbar.splitButton({
                        text: 'AUI Toolbar Split-Button',
                        dropdownItemsContent: aui.dropdown.item({text: 'AUI Dropdown Item'})
                    })
                })
            })
        }))
        .append(aui.form.form({
            action: '#form',
            content: aui.form.formDescription({
                content: 'AUI Form &amp;'
            }) + aui.form.fieldset({
                legendContent: 'Fieldset &amp;',
                content: aui.form.textField({
                    id: 'simple-text-field',
                    labelContent: 'Text Field &amp;'
                }) + aui.form.textareaField({
                    id: 'simple-textarea-field',
                    labelContent: 'Textarea Field &amp;'
                }) + aui.form.passwordField({
                    id: 'simple-password-field',
                    labelContent: 'Password Field &amp;'
                }) + aui.form.fileField({
                    id: 'simple-file-field',
                    labelContent: 'File Field &amp;'
                }) + aui.form.selectField({
                    id: 'simple-select-field',
                    labelContent: 'Select Field &amp;',
                    options: [{
                        text: 'Group 1 &amp;',
                        options: [{
                            text: 'Option 1.1 &amp;',
                            value: '1.1 &amp;'
                        }, {
                            text: 'Option 1.2 &amp;',
                            value: '1.2 &amp;'
                        }]
                    }, {
                        text: 'Option 2 &amp;',
                        value: '2 &amp;'
                    }]
                }) + aui.form.checkboxField({
                    legendContent: 'Checkbox Field &amp;',
                    fields: [{
                        id: 'simple-checkbox-field-1',
                        labelText: 'Checkbox 1 &'
                    }, {
                        id: 'simple-checkbox-field-2',
                        labelText: 'Checkbox 2 &'
                    }, {
                        id: 'simple-checkbox-field-3',
                        labelText: 'Checkbox 3 &'
                    }]
                }) + aui.form.radioField({
                    name: 'simple-radio-field',
                    legendContent: 'Radio Field &amp;',
                    fields: [{
                        id: 'simple-radio-field-1',
                        value: 'radio-1',
                        labelText: 'Radio 1 &'
                    }, {
                        id: 'simple-radio-field-2',
                        value: 'radio-2',
                        labelText: 'Radio 2 &'
                    }, {
                        id: 'simple-radio-field-3',
                        value: 'radio-3',
                        labelText: 'Radio 3 &'
                    }]
                }) + aui.form.valueField({
                    id: 'simple-value-field',
                    labelContent: 'Value Field &amp;',
                    value: 'Value Field &amp;'
                })
            }) + aui.form.buttons({
                content: aui.form.submit({
                    name: 'submit',
                    type: 'primary',
                    text: 'Submit &amp;'
                }) + aui.form.button({
                    name: 'button',
                    text: 'Button &amp;'
                }) + aui.form.linkButton({
                    name: 'link-button',
                    text: 'Link-Button &amp;'
                })
            })
        }));

    function appendSimpleAuiMessage(type) {
        return this.append(aui.message[type]({
            content: '<p>AUI Message (' + type + ')</p>'
        }));
    }
    $simpleContainer.appendAuiMessage = appendSimpleAuiMessage;
    $simpleContainer
        .appendAuiMessage('success')
        .appendAuiMessage('error')
        .appendAuiMessage('hint')
        .appendAuiMessage('info')
        .appendAuiMessage('warning')
        .appendAuiMessage('generic');



    var $complexContainer = AJS.$('<section id="complex-calls"><h3>Complex (all attributes specified)</h3></section>').appendTo($container);

    $complexContainer
        .append(aui.panel({
            id: 'soy-panel',
            content: '<div>AUI Panel &amp;</div>',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section'
        }))
        .append(aui.page.pagePanel({
            id: 'soy-complex-page-panel',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section',
            content:
                aui.page.pagePanelNav({
                    id: 'soy-complex-page-panel-nav',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    tagName: 'section',
                    content: 'AUI Page Panel Nav (using a section element instead of the default of div)'
                }) +
                aui.page.pagePanelContent({
                    id: 'soy-complex-page-panel-content',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    tagName: 'aside',
                    content: 'AUI Page Panel Content (using a aside element instead of the default of section)'
                }) +
                aui.page.pagePanelSidebar({
                    id: 'soy-complex-page-panel-sidebar',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    tagName: 'nav',
                    content: 'AUI Page Panel Sidebar (using a nav element instead of the default of aside)'
                }) +
                aui.page.pagePanelItem({
                    id: 'soy-complex-page-panel-item',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    tagName: 'nav',
                    content: 'AUI Page Panel Item (using a nav element instead of the default of section)'
                })
        }))
        .append(aui.page.pageHeader({
            id: 'soy-complex-page-header',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            content:
                aui.page.pageHeaderImage({
                    id: 'soy-complex-page-header-image',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    content: 'AUI Page Header Image'
                }) +
                aui.page.pageHeaderMain({
                    id: 'soy-complex-page-header-main',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    content: 'AUI Page Header Main'
                }) +
                aui.page.pageHeaderActions({
                    id: 'soy-complex-page-header-actions',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    content: 'AUI Page Header Actions'
                })
        }))
        .append(aui.group.group({
            isSplit: true,
            id: 'soy-group',
            content: aui.group.item({
                content: '<div>AUI Item 1 in AUI Group &amp;</div>',
                id:'soy-item-1',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'section'
            }) + aui.group.item({
                content: '<div>AUI Item 2 in AUI Group &amp;</div>',
                id:'soy-item-2',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'section'
            }),
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section'
        }))
        .append(aui.table({
            id: 'soy-table',
            contentIncludesTbody: true,
            content: '<tbody><tr><td>AUI Table (tbody) &amp;</td></tr></tbody>',
            columnsContent: '<colgroup><col id="complex-table-column"></colgroup>',
            theadContent: '<tr><td>AUI Table (thead) &amp;</td></tr>',
            tfootContent: '<tr><td>AUI Table (tfoot) &amp;</td></tr>',
            captionContent: 'AUI Table (caption) &amp;',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'}
        }))
        .append(aui.tabs({
            isVertical: true,
            isDisabled: true,
            id:'soy-tabs',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section',
            menuItems: [{
                isActive: true,
                url: '#soy-tab-pane-1',
                text: 'Tab 1 &amp;',
                id:'soy-tab-menu-1',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'}
            }, {
                isActive: false,
                url: '#soy-tab-pane-2',
                text: 'Tab 2',
                id:'soy-tab-menu-2',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'}
            }],
            paneContent: aui.tabPane({
                isActive: true,
                content: 'Tab 1 Content - Disabled tabs &amp;',
                id:'soy-tab-pane-1',
                extraClasses: ['extra-class', 'extra-class-2'],
                extraAttributes:'data-attr="extra-attr" data-attr-2="extra-attr-2"',
                tagName: 'section'
            }) + aui.tabPane({
                isActive: false,
                content: 'Tab 2 Content',
                id:'soy-tab-pane-2',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'section'
            })
        }))
        .append(aui.dropdown.parent({
            id: 'soy-dropdown-parent',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section',
            content: aui.dropdown.trigger({
                id: 'soy-dropdown-trigger',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'section',
                accessibilityText: 'AUI Dropdown Trigger &amp;',
                showIcon: false
            }) + aui.dropdown.menu({
                id: 'soy-dropdown-menu',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'section',
                content: aui.dropdown.item({
                    id: 'soy-dropdown-item',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    tagName: 'section',
                    text: 'AUI Dropdown Item &amp;',
                    url: '#dropdown-item'
                })
            })
        }))
        .append(aui.toolbar.toolbar({
            id: 'soy-toolbar',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section',
            content: aui.toolbar.split({
                id: 'soy-toolbar-split-left',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'section',
                split: 'left',
                content: aui.toolbar.group({
                    id: 'soy-toolbar-group',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'},
                    content: aui.toolbar.button({
                        id: 'soy-toolbar-button-1',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        text: 'AUI Button (text) &amp;',
                        url: '#toolbar-button',
                        isPrimary: true,
                        isActive: true
                    }) + aui.toolbar.button({
                        id: 'soy-toolbar-button-2',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        title: 'AUI Button (icon) &amp;',
                        iconClass: 'my-toolbar-icon',
                        url: '#toolbar-button',
                        isPrimary: true,
                        isActive: true
                    }) + aui.toolbar.link({
                        id: 'soy-toolbar-link',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        url: '#link',
                        text: 'AUI Link &amp;'
                    }) + aui.toolbar.dropdown({
                        id: 'soy-toolbar-dropdown',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        text: 'AUI Toolbar Dropdown &amp;',
                        isPrimary: true,
                        dropdownItemsContent: aui.dropdown.item({text: 'AUI Dropdown Item'})
                    }) + aui.toolbar.splitButton({
                        id: 'soy-toolbar-split-button',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        text: 'AUI Toolbar Split-Button &amp;',
                        url: '#split-button',
                        isPrimary: true,
                        dropdownItemsContent: aui.dropdown.item({text: 'AUI Dropdown Item'})
                    })
                })
            })
        }))
        .append(aui.form.form({
            id: 'complex-form',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            action: '#form',
            method: 'GET',
            enctype: 'text/plain',
            isUnsectioned: true,
            isLongLabels: true,
            isTopLabels: true,
            content: aui.form.formDescription({
                id: 'complex-description',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                content: 'AUI Form &amp;'
            }) + aui.form.fieldset({
                id: 'complex-fieldset',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                isInline: true,
                legendContent: 'Fieldset &amp;',
                content: aui.form.textField({
                    id: 'complex-text-field',
                    name: 'complex-text-field-name',
                    labelContent: 'Text Field &amp;',
                    value: 'Text Field &amp;',
                    maxLength: 16,
                    size: 18,
                    isRequired: true,
                    isDisabled: true,
                    descriptionText: 'Text Field &amp;',
                    errorTexts: ['Text Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.textareaField({
                    id: 'complex-textarea-field',
                    name: 'complex-textarea-field-name',
                    labelContent: 'Textarea Field &amp;',
                    value: 'Textarea Field &amp;',
                    rows: 2,
                    cols: 20,
                    isRequired: true,
                    isDisabled: true,
                    descriptionText: 'Textarea Field &amp;',
                    errorTexts: ['Textarea Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.passwordField({
                    id: 'complex-password-field',
                    name: 'complex-password-field-name',
                    labelContent: 'Password Field &amp;',
                    value: 'Password Field &amp;',
                    isRequired: true,
                    isDisabled: true,
                    descriptionText: 'Password Field &amp;',
                    errorTexts: ['Password Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.fileField({
                    id: 'complex-file-field',
                    name: 'complex-file-field-name',
                    labelContent: 'File Field &amp;',
                    value: 'File Field &amp;',
                    isRequired: true,
                    isDisabled: true,
                    descriptionText: 'File Field &amp;',
                    errorTexts: ['File Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.selectField({
                    id: 'complex-select-field',
                    name: 'complex-select-field-name',
                    labelContent: 'Select Field &amp;',
                    options: [{
                        text: 'Group 1 &amp;',
                        options: [{
                            text: 'Option 1.1 &amp;',
                            value: '1.1 &amp;'
                        }, {
                            text: 'Option 1.2 &amp;',
                            value: '1.2 &amp;'
                        }]
                    }, {
                        text: 'Option 2 &amp;',
                        value: '2 &amp;'
                    }],
                    isMultiple: true,
                    size: 3,
                    isRequired: true,
                    isDisabled: true,
                    descriptionText: 'Select Field &amp;',
                    errorTexts: ['Select Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.checkboxField({
                    id: 'complex-checkbox-field',
                    legendContent: 'Checkbox Field &amp;',
                    fields: [{
                        id: 'complex-checkbox-field-1',
                        name: 'complex-checkbox-field-1-name',
                        labelText: 'Checkbox 1 &',
                        isChecked: true,
                        isDisabled: true,
                        descriptionText: 'Checkbox 1 &amp;',
                        errorTexts: ['Checkbox 1 &amp;'],
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'}
                    }, {
                        id: 'complex-checkbox-field-2',
                        name: 'complex-checkbox-field-2-name',
                        labelText: 'Checkbox 2 &'
                    }, {
                        id: 'complex-checkbox-field-3',
                        name: 'complex-checkbox-field-3-name',
                        labelText: 'Checkbox 3 &'
                    }],
                    isRequired: true,
                    isMatrix: true,
                    descriptionText: 'Checkbox Field &amp;',
                    errorTexts: ['Checkbox Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.radioField({
                    id: 'complex-radio-field',
                    name: 'complex-radio-field-name',
                    legendContent: 'Radio Field &amp;',
                    fields: [{
                        id: 'complex-radio-field-1',
                        value: 'radio-1',
                        labelText: 'Radio 1 &',
                        isChecked: true,
                        isDisabled: true,
                        descriptionText: 'Radio 1 &amp;',
                        errorTexts: ['Radio 1 &amp;'],
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'}
                    }, {
                        id: 'complex-radio-field-2',
                        value: 'radio-2',
                        labelText: 'Radio 2 &'
                    }, {
                        id: 'complex-radio-field-3',
                        value: 'radio-3',
                        labelText: 'Radio 3 &'
                    }],
                    isRequired: true,
                    isMatrix: true,
                    descriptionText: 'Radio Field &amp;',
                    errorTexts: ['Radio Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.valueField({
                    id: 'complex-value-field',
                    labelContent: 'Value Field &amp;',
                    value: 'Value Field &amp;',
                    isRequired: true,
                    descriptionText: 'Value Field &amp;',
                    errorTexts: ['Value Field &amp;'],
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                })
            }) + aui.form.buttons({
                alignment: 'right',
                content: aui.form.submit({
                    id: 'complex-form-submit',
                    name: 'submit',
                    text: 'Submit &amp;',
                    isDisabled: true,
                    type: 'primary',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.button({
                    id: 'complex-form-button',
                    name: 'button',
                    text: 'Button &amp;',
                    isDisabled: true,
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }) + aui.form.linkButton({
                    id: 'complex-form-link-button',
                    name: 'link-button',
                    text: 'Link-Button &amp;',
                    href: '#link-button',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                })
            })
        }));

    function appendComplexAuiMessage(type) {
        return this.append(aui.message[type]({
            id: 'soy-message-' + type,
            content: '<p id="soy-message-' + type + '-content">AUI Message (' + type + ') &amp;</p>',
            titleContent: 'AUI Message (' + type + ') &amp;',
            extraClasses: 'extra-class',
            extraAttributes: {'data-attr': 'extra-attr'},
            tagName: 'section',
            isCloseable: true,
            isShadowed: true
        }));
    }
    $complexContainer.appendAuiMessage = appendComplexAuiMessage;
    $complexContainer
        .appendAuiMessage('success')
        .appendAuiMessage('error')
        .appendAuiMessage('hint')
        .appendAuiMessage('info')
        .appendAuiMessage('warning')
        .appendAuiMessage('generic');

    var $headerContainer = AJS.$('<div id="header-soy"><h2>Header Soy (JS) Test</h2></div>').prependTo($container);
    var $headerTest1 = AJS.$(aui.page.header({
            logo: 'aui',
            headerLogoText: 'AUI'
        }));
    var $headerTest2 = AJS.$(aui.page.header({
            logo: 'jira',
            headerLogoText: 'JIRA',
            headerText: 'Testing Visible headerText'
        }));
    var $headerTest3 = AJS.$(aui.page.header({
            logo: 'textonly',
            headerLogoText: 'Text Only',
            headerText: 'visible name'
        }));
    var $headerTest4 = AJS.$(aui.page.header({
            logo: 'aui',
            headerLogoText: 'AUI'
        }));
    var $headerTest5 = AJS.$(aui.page.header({
            logo: 'jira',
            headerLogoText: 'JIRA',
            headerText: 'Testing Visible headerText'
        }));
    var $headerTest6 = AJS.$(aui.page.header({
            logo: 'textonly',
            headerLogoText: 'Text Only',
            headerText: 'visible name'
        }));
    var $headerTest7 = AJS.$(aui.page.header({
            headerLogoText: 'Bamboo',
            headerText: 'Visible Text',
            headerLogoImageUrl: 'img-logo-test40pxhigh.png'
        }));

    // NB this one used for webdriver tests
    var $headerTest9 = AJS.$(aui.page.header({
            id: 'header-customlogo',
            headerLogoText: 'Bamboo',
            headerLogoImageUrl: 'img-logo-test40pxhigh.png'
        }));

    // NB this one used for webdriver tests
    var $headerTest10 = AJS.$(aui.page.header({
            id: 'header-customclassesandattrs',
            logo: 'bamboo',
            headerText: 'This .aui-header has custom classes and attributes',
            headerLogoText: 'Bamboo',
            extraClasses: 'my-awesome-custom-class and-another',
            extraAttributes: {
                'data-foo': 'bar',
                title: 'Custom title'
            },
            primaryNavContent: '<div id="primaryNavContents">primaryNavContents</div>',
            secondaryNavContent: '<div id="secondaryNavContents">secondaryNavContents</div>'
        }));

    // faking the style of a dropdown, we don't need to test this fully as
    // dropdown is only for AppSwitcher and handled by AppSwitcher plugin
    $headerTest1.find('a').addClass('aui-dropdown2-trigger');
    $headerTest2.find('a').addClass('aui-dropdown2-trigger');
    $headerTest3.find('a').addClass('aui-dropdown2-trigger');
    $headerTest9.find('a').addClass('aui-dropdown2-trigger');

    $headerContainer
        .append('<p>Dropdown style only, just testing style.</p>')
        .append($headerTest1)
        .append($headerTest2)
        .append($headerTest3)
        .append('<p>No dropdown affordance.</p>')
        .append($headerTest4)
        .append($headerTest5)
        .append($headerTest6)
        .append('<p>In prod black areas would be transparent so hover would make more sense.</p>')
        .append($headerTest9)
        .append($headerTest7)
        .append($headerTest10)
    ;

    var buttonSetSoy = aui.buttons.buttons({
        id: 'button-set-soy',
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'},
        content:
            aui.buttons.button({
                id: 'button-set-button1',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                isPressed: 'true',
                isDisabled: 'true',
                text: 'Button Text &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button2',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                isPressed: 'true',
                isDisabled: 'true',
                type: 'primary',
                text: 'Button Text &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button3',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'input',
                isPressed: 'true',
                isDisabled: 'true',
                type: 'primary',
                text: 'INPUT button &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button4',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'input',
                inputType: 'submit',
                isPressed: 'true',
                isDisabled: 'true',
                type: 'primary',
                text: 'INPUT submit &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button5',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'a',
                isPressed: 'true',
                isDisabled: 'true',
                type: 'primary',
                text: 'A &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button6',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                isPressed: 'true',
                isDisabled: 'false',
                text: 'Button Text &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button7',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                isPressed: 'true',
                isDisabled: 'false',
                type: 'primary',
                text: 'Button Text &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button8',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'input',
                isPressed: 'true',
                isDisabled: 'false',
                type: 'primary',
                text: 'INPUT type not set &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button9',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'input',
                inputType: 'submit',
                isPressed: 'true',
                isDisabled: 'false',
                type: 'primary',
                text: 'INPUT submit &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button10',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'a',
                isPressed: 'true',
                isDisabled: 'false',
                type: 'primary',
                text: 'A &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button11',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'a',
                isPressed: 'false',
                isDisabled: 'false',
                type: 'link',
                text: 'A &amp;'
            }) +
            aui.buttons.button({
                id: 'button-set-button12',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                tagName: 'a',
                isPressed: 'true',
                isDisabled: 'false',
                type: 'link',
                text: 'A &amp;'
            })
    });

    $complexContainer.append(buttonSetSoy);

    var buttonTextTest = aui.buttons.buttons({
        id: 'button-text-test-soy',
        content:
            aui.buttons.button({
                id: 'button-text-test-1',
                text: 'Button Text'
            }) +
            aui.buttons.button({
                id: 'button-text-test-2',
                tagName: 'a',
                text: 'Button Text'
            }) +
            aui.buttons.button({
                id: 'button-text-test-3',
                tagName: 'input',
                text: 'Button Text'
            }) +
            aui.buttons.button({
                id: 'button-text-test-4',
                tagName: 'input',
                inputType: 'submit',
                text: 'Button Text'
            })
    });
    $complexContainer.append('<p>Testing text in buttons:');
    $complexContainer.append(buttonTextTest);

    var buttonIconTest = aui.buttons.buttons({
        id: 'button-icon-test-soy',
        content:
            aui.buttons.button({
                id: 'button-icon-test-0',
                text: 'Icon Button',
                iconType: 'aui',
                iconText: 'Icon Text',
                iconClass: 'icon-extra-class'
            })           +
            aui.buttons.button({
                id: 'button-icon-test-1',
                text: 'Icon Button (no aui class)',
                iconType: 'custom',
                iconText: 'Custom Icon',
                iconClass: 'icon-custom-class'
            })           +
            aui.buttons.button({
                id: 'button-icon-test-2',
                tagName: 'a',
                text: 'Icon Button',
                iconType: 'aui',
                iconText: 'Icon Text',
                iconClass: 'icon-extra-class'
            }) +
            aui.buttons.button({
                id: 'button-icon-test-3',
                tagName: 'input',
                text: 'Should not have any icon code',
                iconType: 'true',
                iconText: 'Icon Text',
                iconClass: 'icon-extra-class'
            }) +
            aui.buttons.button({
                id: 'button-icon-test-4',
                tagName: 'input',
                inputType: 'submit',
                text: 'Should not have any icon code',
                iconType: 'true',
                iconText: 'Icon Text',
                iconClass: 'icon-extra-class'
            })
    });
    $complexContainer.append('<p>Testing icons in buttons:');
    $complexContainer.append(buttonIconTest);

    var buttonTextTest2 = aui.buttons.buttons({
        id: 'button-dropdown2-test-soy',
        content:
            aui.buttons.button({
                id: 'button-dropdown2-test-1',
                text: 'Dropdown',
                dropdown2Target: 'dropdown'
            }) +
            aui.buttons.button({
                id: 'button-dropdown2-test-2',
                tagName: 'a',
                text: 'Dropdown',
                dropdown2Target: 'dropdown'
            }) +
            aui.buttons.button({
                id: 'button-dropdown2-test-3',
                tagName: 'input',
                text: 'Dropdown',
                dropdown2Target: 'dropdown'
            }) +
            aui.buttons.button({
                id: 'button-dropdown2-test-4',
                tagName: 'input',
                inputType: 'submit',
                text: 'Dropdown',
                dropdown2Target: 'dropdown'
            })
    });
    $complexContainer.append('<p>Testing dropdown button trigger style/attributes:');
    $complexContainer.append(buttonTextTest2);

    var soyTestButtonsSplit = aui.buttons.buttons({
        id: 'split-container',
        content:
            aui.buttons.splitButton({
                splitButtonMain: {
                    id: 'soyTestButtonsSplit-button1',
                    text: 'Split Main',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                },
                splitButtonMore: {
                    id: 'soyTestButtonsSplit-button2',
                    text: 'Split More',
                    dropdown2Target: 'dropdown-button1',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                }
            }) +
            aui.buttons.splitButton({
                splitButtonMain: {
                    id: 'soyTestButtonsSplit-button3',
                    text: 'Primary Split Main',
                    type: 'primary',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-attr': 'extra-attr'}
                },
                splitButtonMore: {
                    id: 'soyTestButtonsSplit-button4',
                    text: 'Primary Split More',
                    type: 'primary',
                    dropdown2Target: 'dropdown-button2',
                    extraClasses: 'extra-class',
                    extraAttributes: {'data-container': '#split-container','data-attr': 'extra-attr'}
                }
            })
    });
    $complexContainer.append('<p>Testing split buttons:');
    $complexContainer.append(soyTestButtonsSplit);

    var soyTestButtonsSubtle = aui.buttons.button({
        id: 'button-test-subtle',
        type: 'subtle',
        text: 'Subtle Button'
    });
    $complexContainer.append('<p>Testing subtle buttons:');
    $complexContainer.append(soyTestButtonsSubtle);


    var toolbar2Soy = aui.toolbar2.toolbar2({
        id: 'soy-toolbar2',
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'},
        content:
            aui.toolbar2.group({
                id: 'soy-toolbar2-group1',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                content:
                    aui.toolbar2.item({
                        id: 'soy-toolbar2-group1-primary',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        item: 'primary',
                        content:
                            aui.buttons.button({
                                id: 'soy-toolbar2-primary-button',
                                extraClasses: 'extra-class',
                                extraAttributes: {'data-attr': 'extra-attr'},
                                text: 'Toolbar Primary'
                            })
                    }) +
                    aui.toolbar2.item({
                        id: 'soy-toolbar2-group1-secondary',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        item: 'secondary',
                        content:
                            aui.buttons.button({
                                id: 'soy-toolbar2-secondary-button',
                                extraClasses: 'extra-class',
                                extraAttributes: {'data-attr': 'extra-attr'},
                                text: 'Toolbar Secondary'
                            })
                    })
            }) +
            aui.toolbar2.group({
                id: 'soy-toolbar2-group2',
                extraClasses: 'extra-class',
                extraAttributes: {'data-attr': 'extra-attr'},
                content:
                    aui.toolbar2.item({
                        item: 'primary',
                        id: 'soy-toolbar2-group2-primary',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        content:
                            aui.buttons.button({
                                id: 'soy-toolbar2-primary-button2',
                                extraClasses: 'extra-class',
                                extraAttributes: {'data-attr': 'extra-attr'},
                                text: 'Second Row Toolbar Primary &amp;'
                            })
                    }) +
                    aui.toolbar2.item({
                        item: 'secondary',
                        id: 'soy-toolbar2-group2-secondary',
                        extraClasses: 'extra-class',
                        extraAttributes: {'data-attr': 'extra-attr'},
                        content:
                            aui.buttons.button({
                                id: 'soy-toolbar2-secondary-button2',
                                extraClasses: 'extra-class',
                                extraAttributes: {'data-attr': 'extra-attr'},
                                text: 'Second Row Toolbar Secondary &amp;'
                            })
                    })
            })
    });

    $complexContainer.append('<p>Testing toolbar2:');
    $complexContainer.append(toolbar2Soy);

    var badgeTest = aui.badges.badge({
        id: 'soy-badge1',
        text: '9',
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'}
    });
    $complexContainer.append('<p>Testing badges</p>');
    $complexContainer.append(badgeTest);

    var labelTest = aui.labels.label({
        id: 'soy-label1',
        text: 'unclickable',
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'}
    }) + aui.labels.label({
        id: 'soy-label2',
        text: 'clickable',
        url: 'http://http://www.atlassian.com/',
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'}
    }) + aui.labels.label({
        id: 'soy-label3',
        text: 'unclickableCloseable',
        isCloseable: true,
        hasTitle: false,
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'}
    }) + aui.labels.label({
        id: 'soy-label4',
        text: 'splitLabel',
        url: 'http://http://www.atlassian.com/',
        isCloseable: true,
        closeIconText: ' CUSTOMTEXT',
        extraClasses: 'extra-class',
        extraAttributes: {'data-attr': 'extra-attr'}
    });
    $complexContainer.append('<p id="labels">Testing labels</p>');
    $complexContainer.append(labelTest);

    var fieldDescriptionContent = aui.form.form({
        action: '#form',
        content: aui.form.formDescription({
            content: 'AUI form with links in the description'
        }) + aui.form.fieldset({
            legendContent: 'Fieldset descriptionContent',
            content: aui.form.textField({
                id: 'descriptionContent-text-field',
                labelContent: 'Text Field, with description content',
                descriptionContent: 'This is some description <a href="http://www.atlassian.com">with a link</a>, and a small image <img src="http://confluence.atlassian.com/images/icons/favicon.png"/>'
            }) + aui.form.textField({
                id: 'descriptionContent-text-field',
                labelContent: 'Text Field, with description text (escaped)',
                descriptionText: 'This is some description <a href="http://www.atlassian.com">with a link</a>, which should be escaped'
            }) + aui.form.textField({
                id: 'descriptionContent-text-field',
                labelContent: 'Text Field, with description content and description text',
                descriptionText: 'This is some description text',
                descriptionContent: 'This is some description <a href="http://www.atlassian.com">with a link</a>, and a small image <img src="http://confluence.atlassian.com/images/icons/favicon.png"/>'
            }) + aui.form.textareaField({
                id: 'descriptionContent-textarea-field',
                labelContent: 'Textarea Field',
                descriptionContent: 'This is some description <a href="http://www.atlassian.com">with a link</a>, and a small image <img src="http://confluence.atlassian.com/images/icons/favicon.png"/>'
            }) + aui.form.passwordField({
                id: 'descriptionContent-password-field',
                labelContent: 'Password Field',
                descriptionContent: 'This is some description <a href="http://www.atlassian.com">with a link</a>, and a small image <img src="http://confluence.atlassian.com/images/icons/favicon.png"/>'
            }) + aui.form.fileField({
                id: 'descriptionContent-file-field',
                labelContent: 'File Field',
                descriptionContent: 'This is some description <a href="http://www.atlassian.com">with a link</a>, and a small image <img src="http://confluence.atlassian.com/images/icons/favicon.png"/>'
            }) + aui.form.valueField({
                id: 'descriptionContent-value-field',
                labelContent: 'Value Field',
                value: 'Value Field',
                descriptionContent: 'This is some description <a href="http://www.atlassian.com">with a link</a>, and a small image <img src="http://confluence.atlassian.com/images/icons/favicon.png"/>'
            })
        })
    });

    $complexContainer.append('<p id="field-descriptionContent">AUI Form field descriptionContent (markup enabled)</p>');
    $complexContainer.append(fieldDescriptionContent);

    AJS.dropDown.Standard({
        useLive: true
    });
});
